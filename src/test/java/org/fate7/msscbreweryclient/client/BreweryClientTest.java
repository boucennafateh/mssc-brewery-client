package org.fate7.msscbreweryclient.client;

import org.fate7.msscbreweryclient.web.model.BeerDto;
import org.fate7.msscbreweryclient.web.model.CustomerDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URI;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BreweryClientTest {

    @Autowired
    BreweryClient breweryClient;

    @Test
    void getBeerById() {
        BeerDto beerDto = breweryClient.getBeerById(UUID.randomUUID());
        assertNotNull(beerDto);
    }

    @Test
    void saveNewBeer(){
        BeerDto beerDto = BeerDto.builder().name("beer").build();
        URI uri = breweryClient.saveNewBeer(beerDto);
        System.out.println(uri);
        assertNotNull(uri);

    }

    @Test
    void updateBeer(){
        BeerDto beerDto = BeerDto.builder().name("beer").build();
        breweryClient.updateBeer(UUID.randomUUID(), beerDto);

    }

    @Test
    void deleteBeer(){
        breweryClient.deleteBeer(UUID.randomUUID());
    }

    @Test
    void getCustomerById() {
        CustomerDto customerDto = breweryClient.getCustomerById(UUID.randomUUID());
        assertNotNull(customerDto);
    }

    @Test
    void saveNewCustomer(){
        CustomerDto customerDto = CustomerDto.builder().name("customer").build();
        URI uri = breweryClient.saveNewCustomer(customerDto);
        System.out.println(uri);
        assertNotNull(uri);

    }

    @Test
    void updateCustomer(){
        CustomerDto customerDto = CustomerDto.builder().name("customer").build();
        breweryClient.updateCustomer(UUID.randomUUID(), customerDto);

    }

    @Test
    void deleteCustomer(){
        breweryClient.deleteCustomer(UUID.randomUUID());
    }
}