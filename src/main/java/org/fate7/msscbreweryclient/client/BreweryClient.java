package org.fate7.msscbreweryclient.client;

import org.fate7.msscbreweryclient.web.model.BeerDto;
import org.fate7.msscbreweryclient.web.model.CustomerDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.events.Event;

import java.net.URI;
import java.util.UUID;

@Component
@ConfigurationProperties(prefix = "sfg.brewery", ignoreUnknownFields = false)
public class BreweryClient {

    private String apihost;
    public final String BEER_V1_PATH = "/api/v1/beer/";
    public final String CUSTOMER_V1_PATH = "/api/v1/customer/";

    private RestTemplate restTemplate;

    public BreweryClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void setApihost(String apihost) {
        this.apihost = apihost;
    }

    public BeerDto getBeerById(UUID id){
        return restTemplate.getForObject(
                apihost + BEER_V1_PATH + id.toString(),
                BeerDto.class
        );
    }

    public URI saveNewBeer(BeerDto beerDto){
        URI uri = restTemplate.postForLocation(
                apihost + BEER_V1_PATH,
                beerDto,
                URI.class
        );
        return uri;
    }
    
    public void updateBeer(UUID id, BeerDto beerDto){
        restTemplate.put(
                apihost + BEER_V1_PATH + id.toString(),
                beerDto
        );
    }

    public void deleteBeer(UUID uuid){
        restTemplate.delete(apihost + BEER_V1_PATH + uuid);
    }

    public CustomerDto getCustomerById(UUID id) {
        return restTemplate.getForObject(
                apihost + CUSTOMER_V1_PATH + id.toString(),
                CustomerDto.class
        );

    }

    public URI saveNewCustomer(CustomerDto customer) {
        return restTemplate.postForLocation(
                apihost + CUSTOMER_V1_PATH,
                customer
        );

    }

    public void updateCustomer(UUID id, CustomerDto customerDto) {
        restTemplate.put(
                apihost + CUSTOMER_V1_PATH + id,
                customerDto
        );
    }

    public void deleteCustomer(UUID id) {
        restTemplate.delete(apihost + CUSTOMER_V1_PATH + id);
    }
}
